namespace :elasticsearch do
  desc 'Elasticsearch のindex作成'
  task :create_index => :environment do
    Film.create_index!
  end

  desc 'Film を Elasticsearch に登録'
  task :import_article => :environment do
    Film.import
  end
end