# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# Director,Actor,Genreを先に生成してからFilmを作成


40.times do
  # Create Actor and Director
  # actor_id should be 1..40
  Actor.create!(name: FFaker::NameJA.name)
  Director.create!(name: FFaker::NameJA.name)
end

5.times do
  Genre.create!(name: FFaker::Music.genre)
end

100.times do

  main_actor_id = [*1..40].sample
  featured_actor_id = [*1..40].sample
  director_id = [*1..40].sample
  genre_id = [*1..5].sample
  main_actor_name = Actor.find(main_actor_id).name
  featured_actor_name = Actor.find(featured_actor_id).name

  Film.create!(
    product_title: FFaker::Movie.title ,
    main_actor: FFaker::NameJA.name,
    featured_actor: FFaker::NameJA.name,
    description: FFaker::Lorem.phrase,
    actor_id: main_actor_id,
    director_id: director_id,
    genre_id: genre_id
  )

end