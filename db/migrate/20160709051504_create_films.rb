class CreateFilms < ActiveRecord::Migration[5.0]
  def change
    create_table :films do |t|
      t.string :main_actor
      t.string :featured_actor
      t.string :product_title
      t.text :description
      t.integer :genre_id
      t.integer :actor_id
      t.integer :director_id
      t.timestamps
    end
  end
end
