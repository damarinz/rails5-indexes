json.array!(@films) do |film|
  json.extract! film, :id, :main_actor, :featured_actor, :product_title, :description, :genre_id, :actor_id
  json.url film_url(film, format: :json)
end
