class Film < ApplicationRecord
  belongs_to :director
  belongs_to :actor
  belongs_to :genre

  include FilmSearchable
end
